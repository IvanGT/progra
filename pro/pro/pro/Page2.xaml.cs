﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pro
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
		public Page2 ()
		{
			InitializeComponent ();
		}
        async private void ValidateUser1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryName.Text) || string.IsNullOrEmpty(entryLastname.Text) || string.IsNullOrEmpty(entryTelefone.Text))
            {
                labelMessage1.TextColor = Color.Red;
                labelMessage1.Text = "Debe Escribir Usuario Y Contraseña";

                await DisplayAlert("Error", "Debe Escribir Usuario Y Contraseña", "Ok");
            }
            else
            {
                labelMessage1.Text = entryName.Text;
                labelMessage2.Text = entryLastname.Text;
                labelMessage3.Text = entryTelefone.Text;
                
            }
        }
    }
}