﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace pro
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}
        async private void ValidateUser(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryUser.Text) || string.IsNullOrEmpty(entryPasword.Text))
            {
                labelMessage.TextColor = Color.Red;
                labelMessage.Text = "Debe Escribir Usuario Y Contraseña";

                await DisplayAlert("Error", "Debe Escribir Usuario Y Contraseña", "Ok");
            }
            else
            {
                labelMessage.TextColor = Color.Green;
                labelMessage.Text = "Su Secion Fue Abierta Correctamente";
                await Navigation.PushAsync(new Page2());
            }
        }
    }
}
